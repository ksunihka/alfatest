import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.io.IOException;
import java.util.List;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.Arrays;

public class SortFile {
    public static void main(String[] args) {
        String fileName = "";
        if (args.length == 2 && args[0].equals("-f")) {
            fileName = args[1];
        } else {
            System.out.println("--- This program for sort formatted string with number from file ---");
            System.out.println("    For sort number, create file and exexute programm with argument -f <pathfile>");
            System.out.println("    Example  java SortFile -f test");
            System.exit(1);
        }

        try {
            //read first line from file
            String line = Files.lines(Paths.get(fileName)).findFirst().get();
            System.out.println("Original string from file -> " + line);

            //parse line as list of integer
            List<Integer> intList = Arrays.stream(line.split(",")).map(Integer::valueOf).collect(Collectors.toList());

            //sort
            Collections.sort(intList);
            System.out.print("Sorted string -> ");
            printList(intList);

            //reverse sort
            Collections.reverse(intList);
            System.out.print("Reverse sorted string -> ");
            printList(intList);

        } catch (IOException e) {
            System.out.println(e);
        }
    }

    //function for print element of list as line, with splitter ,
    public static void printList(List<Integer> intList){
        System.out.println(intList.stream().map(Object::toString).collect(Collectors.joining(",")).toString());
    }

}

