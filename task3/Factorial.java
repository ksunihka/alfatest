import java.math.BigInteger;

public class Factorial {
    //function for calculate factorial non-recirsive
    public static BigInteger calculateFactorial(int n) {
        BigInteger result = BigInteger.ONE;
        for (int i = 1; i <= n; i++) {
            BigInteger y = BigInteger.valueOf(i);
            result = result.multiply(y);
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(calculateFactorial(20));
    }
}